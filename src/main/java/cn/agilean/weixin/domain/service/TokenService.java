package cn.agilean.weixin.domain.service;

import cn.agilean.weixin.domain.model.CheckModel;
import cn.agilean.weixin.domain.utils.HttpKit;
import cn.agilean.weixin.domain.utils.SHA1;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import cn.agilean.redis.domain.model.Ticket;
import cn.agilean.redis.domain.model.Token;
import cn.agilean.redis.domain.repository.TicketRepository;
import cn.agilean.redis.domain.repository.TokenRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
public class TokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenService.class);
    public static final String ACCESSTOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
    public static final String JSAPI_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=";
    
    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private HttpKit httpKit;

    /**
     * 微信开发者验证
     *
     * @param wxAccount
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     */
    @Transactional
    public String validate(String wxToken, CheckModel tokenModel) {
        String signature = tokenModel.getSignature();
        Long timestamp = tokenModel.getTimestamp();
        Long nonce = tokenModel.getNonce();
        String echostr = tokenModel.getEchostr();
        if (signature != null && timestamp != null & nonce != null) {
            String[] str = {wxToken, timestamp + "", nonce + ""};
            Arrays.sort(str); // 字典序排序
            String bigStr = str[0] + str[1] + str[2];
            // SHA1加密    
            String digest = SHA1.encode(bigStr).toLowerCase();
            // 确认请求来至微信
            if (digest.equals(signature)) {
                //最好此处将echostr存起来，以后每次校验消息来源都需要用到
                return echostr;
            }
        }
        return "error";
    }

    /**
     * 获取access_token
     *
     * @return
     */
    public String getAccessToken(String appId, String appSecret) {
        Token token = tokenRepository.showToken(appId);
        if (token != null) {
            return token.getAccessToken();
        } else {
            String jsonStr = httpKit.get(ACCESSTOKEN_URL.concat("&appid=")
                        + appId + "&secret=" + appSecret);
            Map<String, Object> map = JSONObject.parseObject(jsonStr);
            Token newToken = new Token();
            newToken.setAccessToken(map.get("access_token").toString());
            newToken.setExpiresIn((int) map.get("expires_in"));
            tokenRepository.saveToken(newToken, appId);
            return newToken.getAccessToken();
        }
    }

    /**
     * 获得jsapi_ticket（有效期7200秒)
     *
     * @param accessToken
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @throws NoSuchProviderException
     * @throws JSONException
     */
    public String getTicket(String accessToken, String appId) {
        Ticket ticket = ticketRepository.showTicket(appId);
        if (ticket != null) {
            return ticket.getJsapiTicket();
        } else {
            String jsonStr = httpKit.get(JSAPI_TICKET.concat(accessToken));
            Map<String, Object> map = JSONObject.parseObject(jsonStr);
            Ticket newTicket = new Ticket();
            newTicket.setJsapiTicket(map.get("ticket").toString());
            newTicket.setExpiresIn((int) map.get("expires_in"));
            ticketRepository.saveTicket(newTicket, appId);
            return newTicket.getJsapiTicket();
        }
    }

}